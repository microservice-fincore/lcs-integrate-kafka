FROM openjdk:21-jdk-slim

RUN apt update -y && apt-get install wget -y

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /kafka.jar

CMD ["java", "-jar", "kafka.jar"]

EXPOSE 8040