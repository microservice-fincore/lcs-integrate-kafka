package com.fincoreplus.kafkaservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fincoreplus.kafkaservice.model.request.BucketRequest;
import com.fincoreplus.kafkaservice.model.response.Response;
import com.fincoreplus.kafkaservice.publisher.BucketEventsProducer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@RestController
@Slf4j
@AllArgsConstructor
public class BucketEventsController {

    private final BucketEventsProducer bucketEventsProducer;

    @PostMapping("/api/v1/bucket")
    @Operation(summary = "Genereta Bucket")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Get Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class))}),
            @ApiResponse(responseCode = "400",
                    description = "OK",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class))}),
            @ApiResponse(responseCode = "404",
                    description = "OK",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class))}),
            @ApiResponse(responseCode = "503",
                    description = "OK",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class))})
    })
    public ResponseEntity<Response> postLibraryEvent(@RequestBody @Valid BucketRequest bucketRequest
    ) throws JsonProcessingException, ExecutionException, InterruptedException, TimeoutException {
        log.info("libraryEvent : {} ", bucketRequest);
        //invoke the kafka producer
        bucketEventsProducer.sendEvent(bucketRequest);

        //generete dummy data;
//        List<Bucket> listTmp = new ArrayList<>();
//        for(int i =1; i <= 1000; i++){
//            log.info("generete data: "+ i);
//            listTmp.add(new Bucket().builder().acct_no("acct-no-"+ i).build());
//        }
//        BucketRequest tmp = new BucketRequest("123", new Date(), listTmp);
//        bucketEventsProducer.sendEvent(tmp);

        //libraryEventsProducer.sendLibraryEvent_approach2(libraryEvent);
        //libraryEventsProducer.sendLibraryEvent_approach3(libraryEvent);

        log.info("After Sending libraryEvent : ");
        return ResponseEntity.status(HttpStatus.CREATED).body(Response.builder()
                        .statusCode(HttpStatus.CREATED.value())
                        .message("success")
                        .data(bucketRequest)
                .build());
    }
}
