package com.fincoreplus.kafkaservice.config;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.errors.MinioException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Configuration
public class MinioCofig {

    @Value("${app.minio.host}")
    public String host;
    @Value("${app.minio.port}")
    public Integer port;
    @Value("${app.minio.access-key}")
    public String accessKey;
    @Value("${app.minio.secret-key}")
    public String secretKey;
    @Value("${app.minio.bucket}")
    public String repoBucket;

    @Bean
    public MinioClient minioClient() {
        boolean isExist;
        MinioClient minioClient = null;
        try {
            minioClient = MinioClient.builder()
                    .endpoint(host, port, false)
                    .credentials(accessKey, secretKey)
                    .build();

            isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(repoBucket).build());
            if (isExist) {
                log.info("Object Storage bucket {} found...", repoBucket);
            } else {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(repoBucket).build());
            }
        } catch (MinioException | InvalidKeyException | IOException | NoSuchAlgorithmException ex) {
            log.warn("Initiate object storage client failed... {} ", ex);
        }
        return minioClient;
    }

}
