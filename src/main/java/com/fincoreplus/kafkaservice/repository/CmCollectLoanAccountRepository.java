package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.account.CmCollectLoanAccount;
import com.fincoreplus.kafkaservice.model.entity.account.CmCollectLoanAccountKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CmCollectLoanAccountRepository extends JpaRepository<CmCollectLoanAccount, CmCollectLoanAccountKey>, JpaSpecificationExecutor<CmCollectLoanAccount> {
    @Query("SELECT MAX(cm.acctUploadId) FROM CmCollectLoanAccount cm")
    Integer findMaxUploadId();
}
