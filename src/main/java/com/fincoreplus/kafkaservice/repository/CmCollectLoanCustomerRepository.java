package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectLoanCustomer;
import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectLoanCustomerKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CmCollectLoanCustomerRepository extends JpaRepository<CmCollectLoanCustomer, CmCollectLoanCustomerKey>, JpaSpecificationExecutor<CmCollectLoanCustomer> {
    @Query("SELECT MAX(cm.acctUploadId) FROM CmCollectLoanCustomer cm")
    Integer findMaxUploadId();
}
