package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustAddr;
import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustAddrKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CmCollectCustAddrRepository extends JpaRepository<CmCollectCustAddr, CmCollectCustAddrKey>, JpaSpecificationExecutor<CmCollectCustAddr> {
    @Query("SELECT MAX(cma.cmCollectCustAddrKey.caddrDiffid) FROM CmCollectCustAddr cma")
    Integer findMaxDiffId();

    @Query("SELECT MAX(cm.acctUploadId) FROM CmCollectCustAddr cm")
    Integer findMaxUploadId();
}
