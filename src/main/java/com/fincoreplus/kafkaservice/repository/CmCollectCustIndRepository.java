package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustIndContractKey;
import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustIndContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CmCollectCustIndRepository extends JpaRepository<CmCollectCustIndContact, CmCollectCustIndContractKey>, JpaSpecificationExecutor<CmCollectCustIndContact> {

    @Query("SELECT MAX(cmi.key.diffId) FROM CmCollectCustIndContact cmi")
    Integer findMaxDiffId();

    @Query("SELECT MAX(cm.acctUploadId) FROM CmCollectCustIndContact cm")
    Integer findMaxUploadId();

}
