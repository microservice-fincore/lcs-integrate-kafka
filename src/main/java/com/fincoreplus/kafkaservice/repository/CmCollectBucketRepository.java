package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.bucket.CmCollectBucket;
import com.fincoreplus.kafkaservice.model.entity.bucket.CmCollectBucketKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CmCollectBucketRepository extends JpaRepository<CmCollectBucket, CmCollectBucketKey>, JpaSpecificationExecutor<CmCollectBucket> {
    @Query("SELECT MAX(cm.bbnUploadId) FROM CmCollectBucket cm")
    Integer findMaxUploadId();
}
