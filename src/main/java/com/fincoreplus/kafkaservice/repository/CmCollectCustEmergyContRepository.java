package com.fincoreplus.kafkaservice.repository;

import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustEmergyContKey;
import com.fincoreplus.kafkaservice.model.entity.customer.CmCollectCustEmergyCont;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmCollectCustEmergyContRepository extends JpaRepository<CmCollectCustEmergyCont, CmCollectCustEmergyContKey>, JpaSpecificationExecutor<CmCollectCustEmergyCont> {
}
