package com.fincoreplus.kafkaservice.util;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Component
@RequiredArgsConstructor
public class ObjectStorageUtils {

    private final MinioClient minioClient;
    @Value("${app.minio.bucket}")
    public String repoBucket;

    public void uploadSchdLog(File sourceFile, String destObjectName) throws IOException {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(sourceFile));
             ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = bis.read(buffer)) != -1) {
                buf.write(buffer, 0, bytesRead);
            }
            byte[] fileBytes = buf.toByteArray();

            // Now you have the file content as a byte array
            System.out.println("File content as byte array: " + java.util.Arrays.toString(fileBytes));

            try (ByteArrayInputStream bais = new ByteArrayInputStream(fileBytes)) {
//            minioClient.uploadObject(UploadObjectArgs.builder()
//                    .bucket(repoBucket).object(destObjectName).filename(sourceFile.getAbsolutePath()).build());
                minioClient.putObject(PutObjectArgs.builder().bucket(repoBucket).object(destObjectName)
                        .stream(bais, (long) bais.available(), -1)
                        .contentType("application/octet-stream")
                        .build());
            } catch (InvalidKeyException | NoSuchAlgorithmException | IOException |
                     InternalException | ErrorResponseException | InsufficientDataException | InvalidResponseException |
                     ServerException | XmlParserException e) {
                log.error("Error upload file to object storage...", e);
                throw new IOException(e);
            }

        } catch (IOException e) {
            // Handle the exception here
            e.printStackTrace();
        }
    }

    public void deleteSchdLog(String path) throws IOException {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(repoBucket).object(path.replace("\\", "/")).build());
        } catch (NoSuchAlgorithmException | InsufficientDataException | InvalidKeyException |
                ErrorResponseException | ServerException | InternalException | InvalidResponseException |
                XmlParserException ex) {
            log.error("Error delete file " + path.replace("\\", "/") + " from object storage...", ex);
            throw new IOException(ex);
        }
    }
}
