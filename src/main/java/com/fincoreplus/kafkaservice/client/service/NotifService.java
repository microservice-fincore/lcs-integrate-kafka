package com.fincoreplus.kafkaservice.client.service;

import com.fincoreplus.kafkaservice.client.request.MessageSmsRequest;
import com.fincoreplus.kafkaservice.client.request.MessageWaRequest;
import com.fincoreplus.kafkaservice.client.request.MessageWhatsappRequest;
import com.fincoreplus.kafkaservice.client.response.MessageSmsResponse;
import com.fincoreplus.kafkaservice.client.response.MessageWaResponse;
import com.fincoreplus.kafkaservice.model.response.BaseResponse;
import com.fincoreplus.kafkaservice.service.RoundRobinLoadBalancerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.composite.CompositeDiscoveryClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotifService {
//    private final RoundRobinLoadBalancerService loadBalancer;
    private final RestClient restClient;

    public NotifService() {
        restClient = RestClient.builder()
                .baseUrl("http://10.8.11.1:8002")
                .build();
    }

    public BaseResponse<Object> notifWa(MessageWaRequest req) {
//        ServiceInstance instance = loadBalancer.choose("base-service");
//        RestClient restClient = RestClient.builder()
//                .baseUrl(instance.getUri().toString())
//                .build();
        return restClient.post()
                .uri(uriBuilder -> uriBuilder.path("/api/v1/whatsapp/send")
                        .build())
                .accept(MediaType.ALL)
                .body(req)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {
                });
    }

    public BaseResponse<Object> notifWaNew(MessageWhatsappRequest req) {
        return restClient.post()
                .uri(uriBuilder -> uriBuilder.path("/api/v2/whatsapp/send")
                        .build())
                .accept(MediaType.ALL)
                .body(req)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {
                });
    }

    public BaseResponse<Object> notifSms(MessageSmsRequest req) {
//        ServiceInstance instance = loadBalancer.choose("base-service");
//        RestClient restClient = RestClient.builder()
//                .baseUrl(instance.getUri().toString())
//                .build();
        return restClient.post()
                .uri(uriBuilder -> uriBuilder.path("/api/v1/sms/send")
                        .build())
                .accept(MediaType.ALL)
                .body(req)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {
                });
    }
}
