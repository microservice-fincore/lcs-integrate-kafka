package com.fincoreplus.kafkaservice.client.service;

import com.fincoreplus.kafkaservice.model.request.PaymentRequest;
import com.fincoreplus.kafkaservice.model.response.BaseResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.math.BigDecimal;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentService {

    private final RestClient restClient;

    public PaymentService() {
        restClient = RestClient.builder()
                .baseUrl("http://10.8.11.1:8020")
//                .baseUrl("http://localhost:8020")
//                .baseUrl("http://localhost:8020/lcs-service")
                .build();
    }

    public ResponseEntity<BaseResponse> pay(PaymentRequest paymentRequest) {
        ResponseEntity<BaseResponse> response = restClient.post()
                .uri(uriBuilder -> uriBuilder.path("/api/v1/payment/response-payment")
//                        .queryParam("contract-no", paymentRequest.getContract_no())
//                        .queryParam("company-code", paymentRequest.getCompany_code())
//                        .queryParam("ac-interest", paymentRequest.getAc_interest())
//                        .queryParam("ac-principal", paymentRequest.getAc_principal())
//                        .queryParam("ac-penalty", paymentRequest.getAc_penalty())
//                        .queryParam("ac-fee", paymentRequest.getAc_fee())
//                        .queryParam("payment-amount", paymentRequest.getPayment_amount())
//                        .queryParam("response-success", paymentRequest.isResponse_success())
                        .build())
                .accept(MediaType.ALL)
                .body(paymentRequest)
                .retrieve()
                .onStatus(HttpStatusCode::is5xxServerError, (request, response1) -> {
                    response1.getStatusCode();
                })
                .onStatus(HttpStatusCode::is4xxClientError, (request, response1) -> {
                    response1.getStatusCode();
                })
                .onStatus(HttpStatusCode::is3xxRedirection, (request, response1) -> {
                    response1.getStatusCode();
                })
                .toEntity(BaseResponse.class);
        return response;
    }
}
