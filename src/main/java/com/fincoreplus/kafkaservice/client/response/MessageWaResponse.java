package com.fincoreplus.kafkaservice.client.response;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageWaResponse {
    private String waTrxId;

    @NotNull
    private String waCoyId;

    private String waSendTo;

    private String waTemplateCode;

    private String waMessage;

    private boolean waFlag;

    private String waResponseCode;

    private String waDeliveryStatus;

    private int waAttempt;

    private LocalDateTime sendDate;
//
//    private LocalDateTime waUpdatedTimestamp;
}
