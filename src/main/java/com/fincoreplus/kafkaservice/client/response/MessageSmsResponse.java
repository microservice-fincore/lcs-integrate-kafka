package com.fincoreplus.kafkaservice.client.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.time.OffsetDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageSmsResponse {
    private String smsTrxId;

    private String smsCoyId;

    private String apiEndpoint;

    private String smsSendTo;

    private String smsTemplateCode;

    private String smsMessage;

    private String smsFlag;

    private String smsResponseCode;

    private String smsDeliveryStatus;

    private String smsAttempt;

    private OffsetDateTime smsCreatedTimestamp;

    private OffsetDateTime smsUpdatedTimestamp;

    private String smsProvider;

    private Integer smsPrice;

}
