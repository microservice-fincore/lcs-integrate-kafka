package com.fincoreplus.kafkaservice.client.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : base-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2/29/24
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageWhatsappRequest {

    @NotNull
    @NotEmpty
    @NotBlank
    private String companyId;

    private String[] phone_numbers;

    @NotNull
    @NotEmpty
    @NotBlank
    private String template_code;

    private String[] body_text;

    private String trxNo;
}
