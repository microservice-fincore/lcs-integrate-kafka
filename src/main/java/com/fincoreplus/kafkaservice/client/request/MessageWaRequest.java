package com.fincoreplus.kafkaservice.client.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : base-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2/29/24
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageWaRequest {

    @NotNull
    @NotEmpty
    @NotBlank
    private String companyId;

    private String trxid;

    @NotNull
    @NotEmpty
    @NotBlank
    private String wanumber;

    @NotNull
    @NotEmpty
    @NotBlank
    private String message;

    @NotNull
    @NotEmpty
    @NotBlank
    private String waTemplateCode;
}
