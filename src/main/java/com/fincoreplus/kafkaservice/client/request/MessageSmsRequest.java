package com.fincoreplus.kafkaservice.client.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : base-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2/29/24
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageSmsRequest {

    @NotNull
    @NotEmpty
    @NotBlank
    private String phone;

    private String trxid;

    private String companyId;

    private String trxdate;

    @NotNull
    @NotEmpty
    @NotBlank
    private String message;

    private String trxNo;
}
