package com.fincoreplus.kafkaservice.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fincoreplus.kafkaservice.client.request.MessageSmsRequest;
import com.fincoreplus.kafkaservice.client.request.MessageWaRequest;
import com.fincoreplus.kafkaservice.client.request.MessageWhatsappRequest;
import com.fincoreplus.kafkaservice.client.response.MessageSmsResponse;
import com.fincoreplus.kafkaservice.client.response.MessageWaResponse;
import com.fincoreplus.kafkaservice.client.service.NotifService;
import com.fincoreplus.kafkaservice.model.response.BaseResponse;
import com.fincoreplus.kafkaservice.model.response.notification.NotificationResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class NotifConsumer {
    private final ObjectMapper objectMapper;
    private final NotifService notifService;


    @KafkaListener(topics = "${spring.kafka.notification.topic}", groupId = "${spring.kafka.notification.group}")
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
        try {
            NotificationResponse request = objectMapper.readValue(consumerRecord.value(), NotificationResponse.class);

            if (Objects.nonNull(request.getType()) && request.getType().equalsIgnoreCase("44") && Objects.nonNull(request.getData())) {
                MessageWhatsappRequest messageWaRequest = objectMapper.convertValue(request.getData(), MessageWhatsappRequest.class);
                BaseResponse<Object> resp = notifService.notifWaNew(messageWaRequest);
                resp.getMessage();
            } else if (Objects.nonNull(request.getType()) && request.getType().equalsIgnoreCase("42") && Objects.nonNull(request.getData())) {
                MessageSmsRequest messageSmsRequest = objectMapper.convertValue(request.getData(), MessageSmsRequest.class);
                BaseResponse<Object> resp = notifService.notifSms(messageSmsRequest);
                resp.getMessage();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
