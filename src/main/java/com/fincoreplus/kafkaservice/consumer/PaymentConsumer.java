package com.fincoreplus.kafkaservice.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fincoreplus.kafkaservice.client.request.MessageSmsRequest;
import com.fincoreplus.kafkaservice.client.request.MessageWaRequest;
import com.fincoreplus.kafkaservice.client.service.PaymentService;
import com.fincoreplus.kafkaservice.model.request.PaymentRequest;
import com.fincoreplus.kafkaservice.model.response.BaseResponse;
import com.fincoreplus.kafkaservice.model.response.notification.NotificationResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class PaymentConsumer {
    private final ObjectMapper objectMapper;
    private final PaymentService paymentService;
    private final RetryTemplate retryTemplate;

    @KafkaListener(topics = "${spring.kafka.payment.topic}", groupId = "${spring.kafka.payment.group}")
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
        retryTemplate.execute(context -> {
            processMessage(consumerRecord);
            return null;
        });
    }

    private void processMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
        try {
            PaymentRequest request = objectMapper.readValue(consumerRecord.value(), PaymentRequest.class);

            if (Objects.nonNull(request)) {
                ResponseEntity<BaseResponse> resp = this.paymentService.pay(request);
                if (resp.getStatusCode().is2xxSuccessful()) {
                    // success
                    log.info(resp.getStatusCode().toString());
                    log.info(resp.getBody().getMessage().toString());
                }else if (resp.getStatusCode().is4xxClientError()) {
                    // bad request
                    log.error(resp.getStatusCode().toString());
                    log.error(resp.getBody().getError());
                } else {
                    // error
                    log.error(resp.getStatusCode().toString());
                    log.error(resp.getBody().getError());
                    throw new RuntimeException("Payment service failed");
                }
            }
        } catch (Exception ex) {
            log.error("Error processing message: ", ex);
            throw ex;
        }
    }
//
//    @KafkaListener(topics = "${spring.kafka.payment.topic}", groupId = "${spring.kafka.payment.group}")
//    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {
//        try {
//        PaymentRequest request = objectMapper.readValue(consumerRecord.value(), PaymentRequest.class);
//
//        if (Objects.nonNull(request)) {
//            ResponseEntity<BaseResponse> resp = this.paymentService.pay(request);
//            if (resp.getStatusCode().is2xxSuccessful()) {
//                // success
//                log.info(resp.getStatusCode().toString());
//                log.info(resp.getBody().getMessage().toString());
//            }else if (resp.getStatusCode().is4xxClientError()) {
//                // bad request
//                log.error(resp.getStatusCode().toString());
//                log.error(resp.getBody().getError());
//            } else {
//                // error
//                log.error(resp.getStatusCode().toString());
//                log.error(resp.getBody().getError());
//            }
//        }
//    } catch (Exception ex) {
//        log.error("Error processing message: ", ex);
//        throw ex;
//    }
//    }
}
