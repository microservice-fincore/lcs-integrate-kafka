package com.fincoreplus.kafkaservice.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fincoreplus.kafkaservice.model.request.BucketRequest;
import com.fincoreplus.kafkaservice.service.BucketService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class BucketEventsConsumer {
    private final ObjectMapper objectMapper;
    private final BucketService bucketService;

    @KafkaListener(topics = "${spring.kafka.topic}", groupId = "${spring.kafka.group-id}")
    public void onMessage(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {

        try {
            BucketRequest request = objectMapper.readValue(consumerRecord.value(), BucketRequest.class);

            log.info("ConsumerRecord trxId: {}", request.getTrx_id());
            log.info("ConsumerRecord time: {}", request.getTrx_date());
            log.info("ConsumerRecord year: {}", request.getYear());
            log.info("ConsumerRecord month: {}", request.getMonth());

            bucketService.execute(request);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
