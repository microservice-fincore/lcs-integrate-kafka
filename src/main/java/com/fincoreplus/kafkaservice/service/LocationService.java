package com.fincoreplus.kafkaservice.service;

import com.fincoreplus.kafkaservice.model.response.*;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class LocationService {

    private final RoundRobinLoadBalancerService loadBalancer;

    public MstProvinceResponse getProvinceByBpsCode(String bpsCode) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=provinceBpsCode="+bpsCode;
            ResponseEntity<BaseResponse<List<MstProvinceResponse>>> response = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/mstProvince?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstProvinceResponse>>>() {});

            MstProvinceResponse provinceResponse = Objects.nonNull(response.getBody().getData()) ? response.getBody().getData().stream().findAny().orElse(null) : null;

            return provinceResponse;
        } catch (Exception ex) {
            throw new Exception("error get Province: "+ ex.getMessage());
        }
    }

    public MstKabkotaResponse getKabKotaByBpsCode(String bpsCode) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=cityBpsCode="+bpsCode;
            ResponseEntity<BaseResponse<List<MstKabkotaResponse>>> response = RestClient.builder().build().get()
                    .uri( "http://10.8.11.1:8002/api/v1/mstKota?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstKabkotaResponse>>>() {});

            MstKabkotaResponse kabkotaResponse = Objects.nonNull(response.getBody().getData()) ? response.getBody().getData().stream().findAny().orElse(null) : null;

            return kabkotaResponse;
        } catch (Exception ex) {
            throw new Exception("error get Kab kota");
        }
    }

    public MstKecamatanResponse getKecamatanByBpsCode(String bpsCode) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=kecBpsCode="+bpsCode;
            ResponseEntity<BaseResponse<List<MstKecamatanResponse>>> response = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/mstDistrict?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstKecamatanResponse>>>() {});

            MstKecamatanResponse kecamatanResponse = Objects.nonNull(response.getBody().getData()) ? response.getBody().getData().stream().findAny().orElse(null) : null;

            return kecamatanResponse;
        } catch (Exception ex) {
            throw new Exception("error get Kecamatan " + ex.getMessage());
        }
    }

    public MstKelurahanResponse getKelurahanByBpsCode(String bpsCode) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=kelBpsCode="+bpsCode;
            ResponseEntity<BaseResponse<List<MstKelurahanResponse>>> response = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/mstKelurahan?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstKelurahanResponse>>>() {});

            MstKelurahanResponse kelurahanResponse = Objects.nonNull(response.getBody().getData()) ? response.getBody().getData().stream().findAny().orElse(null) : null;

            return kelurahanResponse;
        } catch (Exception ex) {
            throw new Exception("error get kelurahan" + ex.getMessage());
        }
    }

}
