package com.fincoreplus.kafkaservice.service;

import com.fincoreplus.kafkaservice.model.*;
import com.fincoreplus.kafkaservice.model.entity.customer.*;
import com.fincoreplus.kafkaservice.model.request.BucketRequest;
import com.fincoreplus.kafkaservice.model.response.*;
import com.fincoreplus.kafkaservice.repository.*;
import com.fincoreplus.kafkaservice.model.entity.account.CmCollectLoanAccount;
import com.fincoreplus.kafkaservice.model.entity.account.CmCollectLoanAccountKey;
import com.fincoreplus.kafkaservice.model.entity.bucket.CmCollectBucket;
import com.fincoreplus.kafkaservice.model.entity.bucket.CmCollectBucketKey;
import com.fincoreplus.kafkaservice.util.CommonUtil;
import com.fincoreplus.kafkaservice.util.ObjectStorageUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClient;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class BucketService {

    private final CmCollectLoanCustomerRepository cmCollectLoanCustomerRepository;
    private final CmCollectCustIndRepository cmCollectCustIndRepository;
    private final CmCollectBucketRepository cmCollectBucketRepository;
    private final CmCollectLoanAccountRepository cmCollectLoanAccountRepository;
    private final CmCollectCustEmergyContRepository cmCollectCustEmergyContRepository;
    private final CmCollectCustAddrRepository cmCollectCustAddrRepository;
    private final RoundRobinLoadBalancerService loadBalancer;
    private final LocationService locationService;
    private final LookupService lookupService;
    private final ObjectStorageUtils objectStorageUtils;

    public void execute(BucketRequest request) {
        SimpleDateFormat datetimef = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        String path = request.getCompany_code()
                + System.getProperty("file.separator")
                + request.getYear()
                + System.getProperty("file.separator")
                + request.getMonth();

        String pathUpload = request.getCompany_code() + "/" + request.getYear() + "/" + request.getMonth() + "/";

        String fileName = "lcs-integration-kafka-" + request.getTrx_id() + ".log";

        File fileLog = new File(
                System.getProperty("java.io.tmpdir") +
                        System.getProperty("file.separator") +
                        path +
                        System.getProperty("file.separator") +
                        fileName).getAbsoluteFile();

        // Ensure that the directories exist
        fileLog.getParentFile().mkdirs();

        try (BufferedWriter _log = new BufferedWriter(new FileWriter(fileLog))) {
            try {
                CompanyResponse company = this.getCompany(request.getCompany_code());
                if (company == null) {
                    throw new Exception("Company Code Not Found");
                }

                if (Objects.isNull(request.getYear())) {
                    throw new Exception("Year is not null");
                }

                if (Objects.isNull(request.getMonth())) {
                    throw new Exception("Month is not null");
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                sdf.setLenient(false); // Set lenient to false to strictly validate the year

                try {
                    // Try to parse the year. If the year is not valid, it will throw a ParseException.
                    sdf.parse(String.valueOf(request.getYear()));
                } catch (ParseException ex) {
                    // If parsing fails, throw an exception with a custom message.
                    throw new Exception("Invalid Year");
                }

                validateMonth(request.getMonth());

                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " --------------------------------------------------------------" + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Trx Id: " + request.getTrx_id() + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Company: " + company.getOrgName() + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Year: " + request.getYear() + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Month: " + request.getMonth() + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Start Process" + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " --------------------------------------------------------------" + System.lineSeparator());
                int success = 0;
                int failed = 0;
                for (Bucket bucket : request.getData()) {
                    try {
                        this.validation(bucket);
                        this.genereteBucket(bucket, company, request.getYear(), request.getMonth(), _log);
                        success++;
                        _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Account no: " + bucket.getContract().getAcct_no() + " - Success" + System.lineSeparator());
                    } catch (Exception ex) {
                        failed++;
                        _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Account no: " + bucket.getContract().getAcct_no() + " - Error - " + ex.getMessage() + System.lineSeparator());
                    }
                }

                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " --------------------------------------------------------------" + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " total data: " + request.getData().size() + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Success: " + success + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Failed: " + failed + System.lineSeparator());
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Finish Process");

            } catch (Exception ex) {
                _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " Error Process: " + ex.getMessage());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error occurred Generete Bucket");
            log.error("trxId: {}", request.getTrx_id());
            log.error("Company Code: {}", request.getCompany_code());
        }

        try {
            objectStorageUtils.uploadSchdLog(fileLog, pathUpload + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private OrganizationResponse getOrganizations(String code) throws Exception {

        if (Objects.isNull(code)) return null;

        try {
            String filter = "filter=orgCode=" + code;
            ResponseEntity<BaseResponse<List<OrganizationResponse>>> response = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/wos_organizations?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<OrganizationResponse>>>() {
                    });

            List<OrganizationResponse> listOrganization = response.getBody().getData();

            return Objects.nonNull(listOrganization) && listOrganization.size() > 0 ? listOrganization.get(0) : null;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Error get Organizations: " + ex.getMessage());
        }
    }

    private CompanyResponse getCompany(String companyCode) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=orgCode=" + companyCode;
            ResponseEntity<BaseResponse<List<CompanyResponse>>> response = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/company?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<CompanyResponse>>>() {
                    });

            CompanyResponse company = Objects.nonNull(response.getBody().getData()) ? response.getBody().getData().stream().findAny().get() : null;
            return company;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Company Not Found: " + ex.getMessage());
        }
    }

    @Transactional
    public void genereteBucket(Bucket bucket, CompanyResponse company, Integer year, Integer month, BufferedWriter _log) throws Exception {
        try {
            Account account = bucket.getContract();
            Customer customer = bucket.getCustomer();
            EmergencyContact emergencyContact = customer.getCust_emergency_contact();
            List<OtherContacts> otherContacts = bucket.getCustomer().getOther_contacts();

            log.info("generete bucket acct no: " + account.getAcct_no());

            String custId = Objects.nonNull(customer.getCust_id()) ? customer.getCust_id() : UUID.randomUUID().toString().substring(0, 15);

            CmCollectLoanCustomer loanCustomer = cmCollectLoanCustomerRepository.findById(CmCollectLoanCustomerKey.builder()
                    .custId(custId)
                    .custCompanyId(company.getOrgId())
                    .build()).orElse(null);

            if (Objects.isNull(loanCustomer)) {
                loanCustomer = CmCollectLoanCustomer.builder()
                        .cmCollectLoanCustomerKey(CmCollectLoanCustomerKey.builder()
                                .custId(custId)
                                .custCompanyId(company.getOrgId()).build())
                        .custCreatedBy("ADMINPKP")
                        .custCreatedDate(new Date())
                        .build();
//                Integer uploadId = cmCollectLoanCustomerRepository.findMaxUploadId();
//                loanCustomer.setAcctUploadId(Objects.nonNull(uploadId) ? uploadId + 1 : 1);
            } else {
                loanCustomer.setCustUpdatedBy("ADMINPKP");
                loanCustomer.setCustUpdatedDate(new Date());
            }

            loanCustomer.setCustType("IND"); //
            loanCustomer.setCustFullname(account.getAcct_name());
            loanCustomer.setCustDomicileAddr(customer.getCust_address());
            loanCustomer.setCustMainMobileph(customer.getCust_mobile_phone());
            loanCustomer.setCustDomicileLon(customer.getCust_lon());
            loanCustomer.setCustDomicileLat(customer.getCust_lat());
            loanCustomer.setCustDomicileRt(customer.getCust_rt());
            loanCustomer.setCustDomicileRw(customer.getCust_rw());

            String[] phones = Objects.nonNull(customer.getCust_home_phone()) ? customer.getCust_home_phone().split("-") : null;
            if (Objects.nonNull(phones) && phones.length > 1) {
                loanCustomer.setCustMainFixareaph(phones[0]);
                loanCustomer.setCustMainFixphone(phones[1]);
            } else {
                loanCustomer.setCustMainFixphone(customer.getCust_home_phone());
            }


            if (StringUtils.isNotEmpty(customer.getCust_kel_code())) {
                MstKelurahanResponse kelurahanResponse = locationService.getKelurahanByBpsCode(customer.getCust_kel_code());
                loanCustomer.setCustDomicileKelid(Objects.nonNull(kelurahanResponse) ? kelurahanResponse.getKelId() : null);
            }

            if (StringUtils.isNotEmpty(customer.getCust_kec_code())) {
                MstKecamatanResponse kecamatanResponse = locationService.getKecamatanByBpsCode(customer.getCust_kec_code());
                loanCustomer.setCustDomicileKecid(Objects.nonNull(kecamatanResponse) ? kecamatanResponse.getKecId() : null);
            }

            if (StringUtils.isNotEmpty(customer.getCust_kab_kota_code())) {
                MstKabkotaResponse kabkotaResponse = locationService.getKabKotaByBpsCode(customer.getCust_kab_kota_code());
                loanCustomer.setCustDomicileKabkotaid(Objects.nonNull(kabkotaResponse) ? kabkotaResponse.getCityId() : null);
            }

            if (StringUtils.isNotEmpty(customer.getCust_prov_code())) {
                MstProvinceResponse provinceResponse = locationService.getProvinceByBpsCode(customer.getCust_prov_code());
                loanCustomer.setCustDomicileProvid(Objects.nonNull(provinceResponse) ? provinceResponse.getProvinceId() : null);
            }

            cmCollectLoanCustomerRepository.saveAndFlush(loanCustomer);

            MstLookupListResponse addressLookup = lookupService.findLookupListByCode("02", "ADDRESS-TYPE", company.getOrgId());
            CmCollectCustAddr custAddrCriteria = CmCollectCustAddr.builder().build();

            if(Objects.isNull(addressLookup)){
                throw new Exception("Address Lookup not found company ID: "+ company.getOrgId());
            }

//            custAddrCriteria.setCaddrTypeid(addressLookup.getListId());
            custAddrCriteria.setCmCollectCustAddrKey(CmCollectCustAddrKey.builder()
                    .caddrCompanyId(company.getOrgId())
                    .caddrCustId(custId)
                    .build());
            Example<CmCollectCustAddr> example = Example.of(custAddrCriteria);
            List<CmCollectCustAddr> custAddrList = cmCollectCustAddrRepository.findAll(example);

            Integer custAddrDiffId = cmCollectCustAddrRepository.findMaxDiffId();
            CmCollectCustAddr custAddr = CmCollectCustAddr.builder()
                    .cmCollectCustAddrKey(CmCollectCustAddrKey.builder()
                            .caddrCompanyId(company.getOrgId())
                            .caddrDiffid(Objects.nonNull(custAddrDiffId) ? custAddrDiffId + 1 : 1)
                            .caddrCustId(loanCustomer.getCmCollectLoanCustomerKey().getCustId()).build())
                    .caddrTypeid(addressLookup.getListId())
                    .caddrCreatedBy("ADMINPKP")
                    .caddrCreatedDate(new Date())
                    .caddrEffFromdate(new Date())
                    .caddrBillAddr("Y")
                    .build();

//            Integer uploadIdAddr = cmCollectCustAddrRepository.findMaxUploadId();
//            custAddr.setAcctUploadId(Objects.nonNull(uploadIdAddr) ? uploadIdAddr + 1 : 1);

            if (custAddrList.size() > 0) {
                custAddr = custAddrList.get(0);
                custAddr.setCaddrUpdatedBy("ADMINPKP");
                custAddr.setCaddrUpdatedDate(new Date());
            }

            custAddr.setCaddrAddr(loanCustomer.getCustDomicileAddr());
            custAddr.setCaddrKelid(loanCustomer.getCustDomicileKelid());
            custAddr.setCaddrKecid(loanCustomer.getCustDomicileKecid());
            custAddr.setCaddrKabkotaid(loanCustomer.getCustDomicileKabkotaid());
            custAddr.setCaddrProvid(loanCustomer.getCustDomicileProvid());
            custAddr.setCaddrLon(loanCustomer.getCustDomicileLon());
            custAddr.setCaddrLat(loanCustomer.getCustDomicileLat());
            custAddr.setCaddrRt(loanCustomer.getCustDomicileRt());
            custAddr.setCaddrRw(loanCustomer.getCustDomicileRw());
            custAddr.setCaddrTypeid(addressLookup.getListId());
//            custAddr.setCaddrTypeid();

            if(Objects.isNull(custAddr.getCaddrTypeid())){
                throw new Exception("Address Type id is not null");
            }

            SimpleDateFormat datetimef = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
//            _log.write(datetimef.format(new Timestamp(System.currentTimeMillis())) + " TypeId: " + custAddr.getCaddrTypeid() + System.lineSeparator());

            cmCollectCustAddrRepository.saveAndFlush(custAddr);

            //insert contact phone
            MstLookupListResponse phoneLookup = lookupService.findLookupListByCode("42", "CUST_CONT_TYPE", company.getOrgId());
            CmCollectCustIndContact phoneCriteria = CmCollectCustIndContact.builder()
                    .key(CmCollectCustIndContractKey.builder()
                            .companyId(company.getOrgId())
                            .custId(loanCustomer.getCmCollectLoanCustomerKey().getCustId())
                            .build())
                    .typeId(phoneLookup.getListId())
                    .build();

            Example<CmCollectCustIndContact> phoneExample = Example.of(phoneCriteria);
            List<CmCollectCustIndContact> listPhone = cmCollectCustIndRepository.findAll(phoneExample);

            Integer diffIdContact = cmCollectCustIndRepository.findMaxDiffId();
            CmCollectCustIndContact phoneContact = CmCollectCustIndContact.builder()
                    .key(CmCollectCustIndContractKey.builder()
                            .custId(custId)
                            .companyId(company.getOrgId())
                            .diffId(Objects.nonNull(diffIdContact) ? diffIdContact + 1 : 1)
                            .build())
                    .typeId(phoneLookup.getListId())
                    .createdBy("ADMINPKP")
                    .createdDate(new Date())
                    .build();

//            Integer contactUploadId = cmCollectCustIndRepository.findMaxUploadId();
//            phoneContact.setAcctUploadId(Objects.nonNull(contactUploadId) ? contactUploadId + 1 : 1);

            if (listPhone.size() > 0) {
                phoneContact = listPhone.get(0);
                phoneContact.setUpdatedBy("ADMINPKP");
                phoneContact.setUpdatedDate(new Date());
            }

            phoneContact.setText(loanCustomer.getCustMainMobileph());
            cmCollectCustIndRepository.saveAndFlush(phoneContact);


            if (Objects.nonNull(otherContacts)) {
                for (OtherContacts contacts : otherContacts) {
                    MstLookupListResponse contactLookup = lookupService.findLookupListByCode(contacts.getC_type(), "CUST_CONT_TYPE", company.getOrgId());
                    CmCollectCustIndContact contactCriteria = CmCollectCustIndContact.builder()
                            .key(CmCollectCustIndContractKey.builder()
                                    .companyId(company.getOrgId())
                                    .custId(loanCustomer.getCmCollectLoanCustomerKey().getCustId())
                                    .build())
                            .typeId(Objects.nonNull(contactLookup) ? contactLookup.getListId() : null)
                            .build();
                    Example<CmCollectCustIndContact> contactExample = Example.of(contactCriteria);
                    List<CmCollectCustIndContact> listContact = cmCollectCustIndRepository.findAll(contactExample);
                    CmCollectCustIndContact contact = CmCollectCustIndContact.builder()
                            .key(CmCollectCustIndContractKey.builder()
                                    .custId(custId)
                                    .companyId(company.getOrgId())
                                    .build())
                            .createdDate(new Date())
                            .createdBy("ADMINPKP")
                            .build();
                    if (listContact.size() > 0) {
                        contact = listContact.get(0);
                        contact.setUpdatedBy("ADMINPKP");
                        contact.setUpdatedDate(new Date());
                    }
                    contact.setText(contacts.getValue());

                    cmCollectCustIndRepository.saveAndFlush(contact);
                }
            }

            OrganizationResponse organization = this.getOrganizations(account.getAcct_branch_code());
            String cycleAwal = this.getCycle(account, new Date());
            String cycleAkhir = this.getCycle(account, CommonUtil.getFirstDayOfNextMonth(new Date()));

            CmCollectBucket cmCollectBucket = cmCollectBucketRepository.findById(CmCollectBucketKey.builder()
                    .bbnAccountNo(account.getAcct_no())
                    .bbnCompanyId(company.getOrgId())
                    .bbnYear(year)
                    .bbnMonth(month)
                    .build()).orElse(null);

            if (Objects.isNull(cmCollectBucket)) {
                cmCollectBucket = CmCollectBucket.builder()
                        .cmCollectBucketKey(CmCollectBucketKey.builder()
                                .bbnAccountNo(account.getAcct_no())
                                .bbnCompanyId(company.getOrgId())
                                .bbnYear(year)
                                .bbnMonth(month)
                                .build())
                        .bbnFlagPkpc("N")
                        .bbnCreatedBy("ADMINPKP")
                        .bbnCreatedDate(new Date())
                        .bbnAssigned(0)
                        .bbnVisited(0)
                        .bbnAoId(0)
                        .bbnAcCollfee(BigDecimal.ZERO)
                        .bbnCollFee(BigDecimal.ZERO)
                        .bbnAcInt(BigDecimal.ZERO)
                        .bbnAcPrin(BigDecimal.ZERO)
                        .bbnAcPenalty(BigDecimal.ZERO)
                        .bbnOprAcint(BigDecimal.ZERO)
                        .bbnOprAcprin(BigDecimal.ZERO)
                        .bbnDendaTetap(BigDecimal.ZERO)
                        .bbnDendaBerjalan(BigDecimal.ZERO)
                        .build();
                cmCollectBucket.setBbnInstnoAwal(account.getAcct_inst_no());
                cmCollectBucket.setBbnCycleAwal(cycleAwal);
                cmCollectBucket.setBbnDuedateAwal(account.getAcct_due_date());
                cmCollectBucket.setBbnOutsprinAwal(coalesceZero(account.getAcct_outs_prin()));
                cmCollectBucket.setBbnOutsintAwal(coalesceZero(account.getAcct_outs_int()));
//                Integer uploadId = cmCollectBucketRepository.findMaxUploadId();
//                cmCollectBucket.setBbnUploadId(Objects.nonNull(uploadId) ? uploadId + 1 : 1);
            } else {
                cmCollectBucket.setBbnUpdatedBy("ADMINPKP");
                cmCollectBucket.setBbnUpdatedDate(new Date());
            }

            cmCollectBucket.setBbnAmbcPrin(coalesceZero(account.getAcct_ambc_prin()));
            cmCollectBucket.setBbnAmbcInt(coalesceZero(account.getAcct_ambc_int()));
            cmCollectBucket.setBbnOutsintAkhir(coalesceZero(account.getAcct_outs_int()));
            cmCollectBucket.setBbnOutsprinAkhir(coalesceZero(account.getAcct_outs_prin()));
            cmCollectBucket.setBbnAcPrin(coalesceZero(account.getAcct_ac_prin()));
            cmCollectBucket.setBbnAcInt(coalesceZero(account.getAcct_ac_int()));
            cmCollectBucket.setBbnAcPenalty(coalesceZero(account.getAcct_ac_penalty()));
            cmCollectBucket.setBbnCollFee(coalesceZero(account.getAcct_coll_fee()));
            cmCollectBucket.setBbnDuedate(account.getAcct_due_date());
            cmCollectBucket.setBbnDuedateAkhir(account.getAcct_due_date());
//            cmCollectBucket.setBbnInstnoAwal(account.getAcct_inst_no());
            cmCollectBucket.setBbnInstnoAkhir(account.getAcct_inst_no());
            cmCollectBucket.setBbnAcctStatus(Objects.nonNull(account.getAcct_status()) ? account.getAcct_status() : "AC");
            cmCollectBucket.setBbnBebanStatus(Objects.nonNull(account.getAcct_status()) ? account.getAcct_status() : "AC");
            cmCollectBucket.setBbnProductShortname(account.getAcct_prod_code());
            cmCollectBucket.setBbnBisnisUnit(account.getAcct_bisnis_unit());
            cmCollectBucket.setBbnOrganizationId(Objects.nonNull(organization) ? organization.getOrgId() : null);
            cmCollectBucket.setBbnCycleAkhir(cycleAkhir);
            cmCollectBucket.setBbnOprAcprin(account.getAcct_opr_acprin());
            cmCollectBucket.setBbnOprAcint(account.getAcct_opr_acint());
            cmCollectBucket.setBbnPaidDate(account.getAcct_paid_date());

            cmCollectBucketRepository.saveAndFlush(cmCollectBucket);

            CmCollectLoanAccount loanAccount = cmCollectLoanAccountRepository.findById(CmCollectLoanAccountKey.builder()
                    .acctNo(account.getAcct_no())
                    .acctCompanyId(company.getOrgId())
                    .build()).orElse(null);

            if (Objects.isNull(loanAccount)) {
                loanAccount = CmCollectLoanAccount.builder()
                        .id(CmCollectLoanAccountKey.builder()
                                .acctNo(account.getAcct_no())
                                .acctCompanyId(company.getOrgId())
                                .build())
                        .acctCreatedBy("ADMINPKP")
                        .acctCreatedDate(OffsetDateTime.now())
                        .build();
//                Integer uploadId = cmCollectLoanAccountRepository.findMaxUploadId();
//                loanAccount.setAcctUploadId(Objects.nonNull(uploadId) ? uploadId + 1 : 1);
            } else {
                loanAccount.setAcctUpdatedBy("ADMINPKP");
                loanAccount.setAcctUpdatedDate(OffsetDateTime.now());
            }

            loanAccount.setAcctActiveDate(Objects.nonNull(account.getAcct_active_date()) ? CommonUtil.convertToLocalDate(account.getAcct_active_date()) : null);
            loanAccount.setAcctCustId(custId);
            loanAccount.setAcctName(account.getAcct_name());
            loanAccount.setAcctCurrency("IDR");
            loanAccount.setAcctCustType("IND");
            loanAccount.setAcctCreatedDate(OffsetDateTime.now());
            loanAccount.setAcctVersionNo(0);
            loanAccount.setAcct_ambc_prnc(account.getAcct_ambc_prin());
            loanAccount.setAcct_ambc_intr(account.getAcct_ambc_int());
            loanAccount.setAcctInstallAmt(account.getAcct_ambc_prin().add(account.getAcct_ambc_prin()));
            loanAccount.setAcctLoanAmt(account.getAcct_loan_amt());
            loanAccount.setAcctProjectAmt(account.getAcct_loan_amt());
            loanAccount.setAcctJmlAngs(account.getAcct_top());
            loanAccount.setAcct_install_no(account.getAcct_inst_no());

            cmCollectLoanAccountRepository.saveAndFlush(loanAccount);

            if (Objects.nonNull(emergencyContact)) {
                String emerCustId = Objects.nonNull(emergencyContact.getCust_id()) ? emergencyContact.getCust_id() : UUID.randomUUID().toString().substring(0, 15);
                CmCollectCustEmergyCont emergyCont = cmCollectCustEmergyContRepository.findById(CmCollectCustEmergyContKey.builder()
                        .companyId(company.getOrgId())
                        .custId(emerCustId)
                        .build()).orElse(null);

                if (Objects.isNull(emergyCont)) {
                    emergyCont = CmCollectCustEmergyCont.builder()
                            .key(CmCollectCustEmergyContKey.builder()
                                    .custId(emerCustId)
                                    .companyId(company.getOrgId())
                                    .build())
                            .createdBy("ADMINPKP")
                            .createdDate(new Date())
                            .build();
                } else {
                    emergyCont.setUpdatedBy("ADMINPKP");
                    emergyCont.setUpdatedDate(new Date());
                }

                emergyCont.setAddress(emergencyContact.getCust_address());
                emergyCont.setRt(emergencyContact.getCust_rt());
                emergyCont.setRw(emergencyContact.getCust_rw());
                emergyCont.setCreatedDate(new Date());
                emergyCont.setCreatedBy("ADMINPKP");
                emergyCont.setMobilePhone(emergencyContact.getCust_mobile_phone());
                emergyCont.setPhone(emergencyContact.getCust_phone());

                if (StringUtils.isNotEmpty(emergencyContact.getCust_kel_code())) {
                    MstKelurahanResponse kelurahanResponse = locationService.getKelurahanByBpsCode(emergencyContact.getCust_kel_code());
                    emergyCont.setKelId(Objects.nonNull(kelurahanResponse) ? kelurahanResponse.getKelId() : null);
                }

                if (StringUtils.isNotEmpty(emergencyContact.getCust_kec_code())) {
                    MstKecamatanResponse kecamatanResponse = locationService.getKecamatanByBpsCode(emergencyContact.getCust_kec_code());
                    emergyCont.setKecId(Objects.nonNull(kecamatanResponse) ? kecamatanResponse.getKecId() : null);
                }

                if (StringUtils.isNotEmpty(emergencyContact.getCust_kab_kota_code())) {
                    MstKabkotaResponse kabkotaResponse = locationService.getKabKotaByBpsCode(emergencyContact.getCust_kab_kota_code());
                    emergyCont.setKotaId(Objects.nonNull(kabkotaResponse) ? kabkotaResponse.getCityId() : null);
                }

                if (StringUtils.isNotEmpty(emergencyContact.getCust_prov_code())) {
                    MstProvinceResponse provinceResponse = locationService.getProvinceByBpsCode(emergencyContact.getCust_prov_code());
                    emergyCont.setProvId(Objects.nonNull(provinceResponse) ? provinceResponse.getProvinceId() : null);
                }

                if (StringUtils.isNotEmpty(emergencyContact.getCust_relation_code())) {
                    MstLookupListResponse lookupListResponse = lookupService.findLookupListByCode(emergencyContact.getCust_relation_code(), "EMERG-RELATION", company.getOrgId());
                    emergyCont.setRelId(Objects.nonNull(lookupListResponse) ? lookupListResponse.getListId() : null);
                }

                cmCollectCustEmergyContRepository.saveAndFlush(emergyCont);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    private String getCycle(Account account, Date date) throws Exception {
        //kontrak lunas/preterm
        if(Objects.isNull(account.getAcct_due_date())) return "CL";

        Long day = CommonUtil.diffInDays(account.getAcct_due_date(), date);
        ServiceInstance instance = loadBalancer.choose("lcs-service");

        if (instance == null) {
            throw new Exception("LCS Service Not Found");
        }

        try {
            ResponseEntity<BaseResponse<List<CyclesResponse>>> response = RestClient.builder().build().get()
//                .uri(instance.getUri().toString() + "/lcs-service/api/v1/cycles")
                    .uri("http://10.8.11.1:8020/api/v1/cycles")
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<CyclesResponse>>>() {
                    });

            List<CyclesResponse> listCycles = response.getBody().getData();
            CyclesResponse cyclesResponse = listCycles.stream().filter(item -> {
                return day >= item.getCycleOvdStart() && day <= item.getCycleOvdEnd() && Objects.isNull(item.getCycleEffTo());
            }).findFirst().orElse(null);

            return Objects.nonNull(cyclesResponse) ? cyclesResponse.getCycleCode() : "C0";
        } catch (Exception ex) {
            throw new Exception("Get Cycle Error: " + ex.getMessage());
        }
    }

    private void validation(Bucket bucket) throws Exception {
        Account account = bucket.getContract();
        Customer customer = bucket.getCustomer();

        this.notNullPostiveValidation("acct_loan_amt", account.getAcct_loan_amt());
        this.notNullGreaterThenZeroValidation("acct_top", account.getAcct_top());
//        this.notNullGreaterThenZeroValidation("acct_inst_no", account.getAcct_inst_no());
        this.notNullPostiveValidation("acct_ambc_int", account.getAcct_ambc_int());
        this.notNullPostiveValidation("acct_ambc_prin", account.getAcct_ambc_prin());
        this.notNullPostiveValidation("acct_outs_int", account.getAcct_outs_int());
        this.notNullPostiveValidation("acct_outs_prin", account.getAcct_outs_prin());

    }

    private void notNullPostiveValidation(String column, BigDecimal amount) throws Exception {
        if (Objects.isNull(amount)) {
            throw new Exception(column + " is not null");
        }

        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new Exception(column + "must be greather then 0");
        }
    }

    private void notNullPostiveValidation(String column, Integer amount) throws Exception {
        if (Objects.isNull(amount)) {
            throw new Exception(column + " is not null");
        }

        if (amount < 0) {
            throw new Exception(column + "must be greather then equal 0");
        }
    }

    private void notNullGreaterThenZeroValidation(String column, Integer amount) throws Exception {
        if (Objects.isNull(amount)) {
            throw new Exception(column + " is not null");
        }

        if (amount < 1) {
            throw new Exception(column + "must be greather then 0");
        }
    }

    public static void validateMonth(Integer month) throws Exception {

        if (month < 1 || month > 12) {
            throw new Exception("Invalid Month");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        sdf.setLenient(false); // Set lenient to false to strictly validate the month

        try {
            // Try to parse the month. If the month is not valid, it will throw a ParseException.
            sdf.parse(String.valueOf(month));
        } catch (ParseException ex) {
            // If parsing fails, throw an exception with a custom message.
            throw new Exception("Invalid Month");
        }
    }

    public BigDecimal coalesceZero(BigDecimal amt){
        return Objects.nonNull(amt) ? amt : BigDecimal.ZERO;
    }

}
