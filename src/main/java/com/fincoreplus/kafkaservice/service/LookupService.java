package com.fincoreplus.kafkaservice.service;

import com.fincoreplus.kafkaservice.model.response.MstLookupHdrResponse;
import com.fincoreplus.kafkaservice.model.response.MstLookupListResponse;
import com.fincoreplus.kafkaservice.model.response.BaseResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class LookupService {

    private final RoundRobinLoadBalancerService loadBalancer;

    public List<MstLookupListResponse> findLookupListByNameHdr(String lookupName, Integer company) throws Exception {
        ServiceInstance instance = loadBalancer.choose("base-service");
        if (instance == null) {
            throw new Exception("Base Service Not Found");
        }

        try {
            String filter = "filter=lookupName="+lookupName+",lookupCompanyId="+ company;
            ResponseEntity<BaseResponse<List<MstLookupHdrResponse>>> responseHdr = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/mst_lookup_hdr?" + filter)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstLookupHdrResponse>>>() {});

            List<MstLookupHdrResponse> listHdr = Objects.nonNull(responseHdr.getBody().getData()) ? responseHdr.getBody().getData() : null;

            Integer hdrId = listHdr.size() > 0 ? listHdr.get(0).getLookupId() : null;
            if (hdrId == null) return new ArrayList<>();

            String filterDtl = "filter=lookupId="+ hdrId + ",listCompanyId="+company;
            ResponseEntity<BaseResponse<List<MstLookupListResponse>>> responseList = RestClient.builder().build().get()
                    .uri("http://10.8.11.1:8002/api/v1/mst_lookup_list?" + filterDtl)
                    .retrieve()
                    .toEntity(new ParameterizedTypeReference<BaseResponse<List<MstLookupListResponse>>>() {});

            List<MstLookupListResponse> listData = Objects.nonNull(responseList.getBody().getData()) ? responseList.getBody().getData() : null;


            return listData;
        } catch (Exception ex) {
            throw new Exception("Error get lookup HDR: "+ ex.getMessage());
        }
    }

    public MstLookupListResponse findLookupListByCode(String code, String lookupName, Integer company) throws Exception {
       try {
           List<MstLookupListResponse> listLookup = this.findLookupListByNameHdr(lookupName, company);
           MstLookupListResponse lookupListResponse = listLookup.stream().filter(item -> {
               return item.getListCode().equals(code);
           }).findFirst().orElse(null);

           return lookupListResponse;
       } catch (Exception ex) {
            throw new Exception("Error get lookup List: "+ ex.getMessage());
        }
    }

}
