package com.fincoreplus.kafkaservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class RoundRobinLoadBalancerService {

    private final DiscoveryClient discoveryClient;
    private final AtomicInteger position = new AtomicInteger(0);

    public RoundRobinLoadBalancerService(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    public ServiceInstance choose(String serviceId) {
        List<ServiceInstance> instances = discoveryClient.getInstances(serviceId);
        log.info("instalnces: {}", instances);
        if (instances.isEmpty()) {
            return null;
        }

        // Get the index of the next instance to use
        int index = position.updateAndGet(current -> (current + 1) % instances.size());
        return instances.get(index);
    }
}