package com.fincoreplus.kafkaservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    private String cust_id;
    private String cust_address;
    private String cust_home_phone;
    private String cust_mobile_phone;
    private String cust_rt;
    private String cust_rw;
    private String cust_kel_code; //bps code
    private String cust_kec_code; //bps code
    private String cust_kab_kota_code;  //bps code
    private String cust_prov_code;  //bps code
    private BigDecimal cust_lon;
    private BigDecimal cust_lat;
    private List<OtherContacts> other_contacts;
    private EmergencyContact cust_emergency_contact;
}
