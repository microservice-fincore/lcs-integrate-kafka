package com.fincoreplus.kafkaservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmergencyContact {
    private String cust_id;
    private String cust_name;
    private String cust_address;
    private String cust_rt;
    private String cust_rw;
    private String cust_kel_code; //bps code
    private String cust_kec_code; //bps code
    private String cust_kab_kota_code;  //bps code
    private String cust_prov_code;  //bps code
    private String cust_phone;
    private String cust_mobile_phone;
    private String cust_relation_code; //lookup
}
