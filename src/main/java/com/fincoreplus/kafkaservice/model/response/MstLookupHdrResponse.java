package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Objects;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class MstLookupHdrResponse {

    private Integer lookupId;

    private String lookupName;

    private String lookupDescription;

    private String lookupType;

    private String lookupDataType;

    private IdNameResponse lookupCompanyId;

}