package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MstKecamatanResponse {

    private Integer kecId;

    private String kecName;

    private IdNameResponse city;

    private Integer kecSeq;

    private String kecBpsCode;

}