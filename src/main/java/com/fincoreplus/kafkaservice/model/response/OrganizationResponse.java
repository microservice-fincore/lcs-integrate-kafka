package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationResponse {

    private Integer orgId;

    private String orgCode;

    private String orgName;

    private String orgDescription;
}