package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MstLookupListResponse {

    private Integer listId;

    private Map lookupId;

    private String listCode;

    private String listDesc;

    private String listPlatform;

    private String listGroupConst;

    private Integer listParentId;

    private IdNameResponse parentList;

    private Date listEffFromdate;

    private Date listEffTodate;

    private Integer listSeqnNo;

    private UUID listUuid;

    private String listContent;

    private String listViewName;

    private String listPrioFlag;

    private IdNameResponse listCompanyId;
}