package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MstKelurahanResponse {

    private Integer kelId;

    private String kelName;

    private Integer kelSeq;

    private String kelBpsCode;

    private String kelZipcode;

    private IdNameResponse district;

}