package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MstKabkotaResponse {

    private Integer cityId;

    private String cityName;

    private IdNameResponse province;

    private Integer citySeq;

    private String cityBpsCode;

}