package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompanyResponse {

    private Integer orgverId;
    private Integer orgverVersionNo;
    private Integer orgverLevelId;
    private Date orgverEffFromdate;
    private Date orgverEffTodate;
    private String orgverIsHeadoffice;
    private Integer orgverStructparentOrgId;
    private String orgverGlCode;
    private String orgverIsArea;
    private Integer orgId;
    private String orgCode;
    private String orgName;
    private String orgDescription;
    private Integer orglevelId;
    private String orglevelCode;
    private Integer orglevelNumber;
    private String orglevelDescription;
    private String parentCode;
    private Integer orgoutletId;
    private String orgoutletCode;
    private String orgoutletDescription;
    private Integer orgoutletSeqnNo;
    private Date orgoutletStartDate;
    private Date orgoutletEndDate;
    private Integer locationId;
    private String locationCode;
    private String locationName;
    private Date locationEffFromdate;
    private Date locationEffTodate;
    private String locationAlamat;
    private String locationRt;
    private String locationRw;
    private Integer locationKecId;
    private Integer locationKabkotaId;
    private Integer locationProvId;
    private String locationKodepos;
    private String locationNpwpNo;
    private String locationKantorKpp;
    private Integer locationKelId;
    private String locationPemotongTax;
    private BigDecimal locationCostIndex;
    private BigDecimal locationLat;
    private BigDecimal locationLon;
    private Integer locationTzlistId;
    private String cityName;
    private String cityBpsCode;
    private String kecName;
    private String kecBpsCode;
    private String kelName;
    private String kelBpsCode;
    private String provinceName;
    private String provinceBpsCode;
}