package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CyclesResponse {
    private String cycleCode;
    private Integer cycleCompanyId;
    private String cycleName;
    private String cycleDesc;
    private Integer cycleOvdStart;
    private Integer cycleOvdEnd;
    private String cycleStatus;
    private String cycleFlag;
    private Integer cycleRptPoss;
    private Integer cycleRollingCpcCrc;
    private String cyclePcFlag;
    private String cycleSwFlag;
    private String cycleAlir;
    private Date cycleEffFrom;
    private Date cycleEffTo;
}