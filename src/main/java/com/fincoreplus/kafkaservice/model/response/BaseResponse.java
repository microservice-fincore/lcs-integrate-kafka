package com.fincoreplus.kafkaservice.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "BaseResponse model")
public class BaseResponse<T> implements Serializable {
    @Schema(description = "Response timestamp")
    private Date timestamp;
    @Schema(description = "Http status code")
    private Integer statusCode;
    @Schema(description = "error message, empty if no errors")
    private String error;
    @Schema(description = "error reason or message provided")
    private Object message;
    @Schema(description = "Number of total records")
    private int ttlRecords;
    @Schema(description = "Number of total pages")
    private int ttlPages;
    @Schema(description = "page number")
    private int pageNo;
    @Schema(description = "number of records in this page")
    private int pageRecords;
    @Schema(description = "response payload")
    private T data;
}
