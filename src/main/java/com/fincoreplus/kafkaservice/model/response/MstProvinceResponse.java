package com.fincoreplus.kafkaservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MstProvinceResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer provinceId;

    private String provinceName;

    private Integer provinceSeq;

    private String provinceBpsCode;

}
