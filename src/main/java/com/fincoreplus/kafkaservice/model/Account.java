package com.fincoreplus.kafkaservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String acct_no;
    private String acct_name;
    private String acct_status;
    private Date acct_active_date;
    private Integer acct_inst_no;
    private Integer acct_top;
    private Date acct_due_date;
    private BigDecimal acct_ambc_prin;
    private BigDecimal acct_ambc_int;
    private BigDecimal acct_outs_int;
    private BigDecimal acct_outs_prin;
    private BigDecimal acct_ac_prin;
    private BigDecimal acct_ac_int;
    private BigDecimal acct_ac_penalty;
    private BigDecimal acct_coll_fee;
    private BigDecimal acct_opr_acprin;
    private BigDecimal acct_opr_acint;
    private String acct_prod_code;
    private String acct_bisnis_unit;
    private String acct_branch_code;
    private BigDecimal acct_loan_amt;
    private Date acct_paid_date;
}
