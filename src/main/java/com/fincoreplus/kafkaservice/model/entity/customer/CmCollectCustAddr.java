package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cm_collect_cust_addr", schema = "staging")
public class CmCollectCustAddr {


    @EmbeddedId
    private CmCollectCustAddrKey cmCollectCustAddrKey;

    @Column(name = "caddr_typeid", unique = false, nullable = false, insertable = true, updatable = true)
    private Integer caddrTypeid;

    @Column(name = "caddr_addr", unique = false, nullable = true, insertable = true, updatable = true, length = 255)
    private String caddrAddr;

    @Column(name = "caddr_rt", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String caddrRt;

    @Column(name = "caddr_rw", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String caddrRw;

    @Column(name = "caddr_kecid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer caddrKecid;

    @Column(name = "caddr_kabkotaid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer caddrKabkotaid;

    @Column(name = "caddr_provid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer caddrProvid;

    @Column(name = "caddr_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String caddrZip;

    @Column(name = "caddr_lat", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal caddrLat;

    @Column(name = "caddr_lon", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal caddrLon;

    @Column(name = "caddr_eff_fromdate", unique = false, nullable = false, insertable = true, updatable = true)
    private Date caddrEffFromdate;

    @Column(name = "caddr_eff_todate", unique = false, nullable = true, insertable = true, updatable = true)
    private Date caddrEffTodate;

    @CreatedBy
    @Column(name = "caddr_created_by", unique = false, nullable = false, insertable = true, updatable = true, length = 30)
    private String caddrCreatedBy;

    @CreatedDate
    @Column(name = "caddr_created_date", unique = false, nullable = false, insertable = true, updatable = true)
    private Date caddrCreatedDate;

    @LastModifiedBy
    @Column(name = "caddr_updated_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String caddrUpdatedBy;

    @LastModifiedDate
    @Column(name = "caddr_updated_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date caddrUpdatedDate;

    @Column(name = "caddr_kelid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer caddrKelid;

    @Column(name = "caddr_bill_addr", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String caddrBillAddr;

    @Column(name = "acct_upload_id", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private Integer acctUploadId;

}
