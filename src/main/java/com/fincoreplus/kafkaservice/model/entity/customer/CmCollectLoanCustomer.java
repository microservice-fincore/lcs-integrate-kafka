package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cm_collect_loan_customer", schema = "staging")
public class CmCollectLoanCustomer {

    @EmbeddedId
    private CmCollectLoanCustomerKey cmCollectLoanCustomerKey;

    @Column(name = "cust_fullname", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
    private String custFullname;

    @Column(name = "cust_type", unique = false, nullable = false, insertable = true, updatable = true, length = 3)
    private String custType;

    @Column(name = "cust_gol_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custGolId;

    @Column(name = "cust_relation_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custRelationId;

    @Column(name = "cust_npwp_no", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String custNpwpNo;

    @Column(name = "cust_main_addr", unique = false, nullable = true, insertable = true, updatable = true, length = 255)
    private String custMainAddr;

    @Column(name = "cust_main_rt", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custMainRt;

    @Column(name = "cust_main_rw", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custMainRw;

    @Column(name = "cust_main_kecid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custMainKecid;

    @Column(name = "cust_main_kabkotaid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custMainKabkotaid;

    @Column(name = "cust_main_provid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custMainProvid;

    @Column(name = "cust_main_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custMainZip;

    @Column(name = "cust_main_lat", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal custMainLat;

    @Column(name = "cust_main_lon", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal custMainLon;

    @Column(name = "cust_main_fixareaph", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custMainFixareaph;

    @Column(name = "cust_main_fixphone", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String custMainFixphone;

    @Column(name = "cust_mainfax_area_code", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custMainfaxAreaCode;

    @Column(name = "cust_mainfax_no", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String custMainfaxNo;

    @Column(name = "cust_main_mobileph", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String custMainMobileph;

    @Column(name = "cust_main_email", unique = false, nullable = true, insertable = true, updatable = true, length = 60)
    private String custMainEmail;

    @Column(name = "cust_industrysector_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custIndustrysectorId;

    @Column(name = "cust_subsector_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custSubsectorId;

    @Column(name = "cust_created_by", unique = false, nullable = false, insertable = true, updatable = true)
    private String custCreatedBy;

    @CreatedDate
    @Column(name = "cust_created_date", unique = false, nullable = false, insertable = true, updatable = true)
    private Date custCreatedDate;

    @Column(name = "cust_updated_by", unique = false, nullable = false, insertable = true, updatable = true)
    private String custUpdatedBy;

    @LastModifiedDate
    @Column(name = "cust_updated_date", unique = false, nullable = false, insertable = true, updatable = true)
    private Date custUpdatedDate;

    @Column(name = "cust_main_kelid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custMainKelid;

    @Column(name = "cust_domicile_addr", unique = false, nullable = true, insertable = true, updatable = true, length = 255)
    private String custDomicileAddr;

    @Column(name = "cust_domicile_rt", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custDomicileRt;

    @Column(name = "cust_domicile_rw", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custDomicileRw;

    @Column(name = "cust_domicile_kelid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custDomicileKelid;

    @Column(name = "cust_domicile_kecid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custDomicileKecid;

    @Column(name = "cust_domicile_kabkotaid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custDomicileKabkotaid;

    @Column(name = "cust_domicile_provid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custDomicileProvid;

    @Column(name = "cust_domicile_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String custDomicileZip;

    @Column(name = "cust_domicile_lat", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal custDomicileLat;

    @Column(name = "cust_domicile_lon", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal custDomicileLon;

    @Column(name = "cust_categry_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer custCategryId;

    @Column(name = "cust_agama", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    private String custAgama;

    @Column(name = "cust_occupation", unique = false, nullable = true, insertable = true, updatable = true, length = 255)
    private String custOccupation;

    @Column(name = "cust_gender", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String custGender;

    @Column(name = "cust_birthdate", unique = false, nullable = true, insertable = true, updatable = true)
    private Date custBirthdate;

    @Column(name = "acct_upload_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer acctUploadId;

}
