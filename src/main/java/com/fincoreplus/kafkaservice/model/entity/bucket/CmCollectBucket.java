package com.fincoreplus.kafkaservice.model.entity.bucket;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cm_collect_bucket", schema = "staging")
public class CmCollectBucket {

    @EmbeddedId
    private CmCollectBucketKey cmCollectBucketKey;

    @Column(name = "bbn_organization_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnOrganizationId;

    @Column(name = "bbn_acct_lob", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnAcctLob;

    @Column(name = "bbn_outsprin_awal", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOutsprinAwal = BigDecimal.ZERO;

    @Column(name = "bbn_outsint_awal", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOutsintAwal = BigDecimal.ZERO;

    @Column(name = "bbn_duedate_awal", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnDuedateAwal;

    @Column(name = "bbn_instno_awal", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnInstnoAwal;

    @Column(name = "bbn_cycle_awal", unique = false, nullable = true, insertable = true, updatable = true, length = 3)
    private String bbnCycleAwal;

    @Column(name = "bbn_polabayar_awal", unique = false, nullable = true, insertable = true, updatable = true, length = 3)
    private String bbnPolabayarAwal;

    @Column(name = "bbn_outsprin_akhir", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOutsprinAkhir = BigDecimal.ZERO;

    @Column(name = "bbn_outsint_akhir", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOutsintAkhir = BigDecimal.ZERO;

    @Column(name = "bbn_duedate_akhir", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnDuedateAkhir;

    @Column(name = "bbn_instno_akhir", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnInstnoAkhir;

    @Column(name = "bbn_cycle_akhir", unique = false, nullable = true, insertable = true, updatable = true, length = 3)
    private String bbnCycleAkhir;

    @Column(name = "bbn_polabayar_akhir", unique = false, nullable = true, insertable = true, updatable = true, length = 3)
    private String bbnPolabayarAkhir;

    @Column(name = "bbn_acct_status", unique = false, nullable = false, insertable = true, updatable = true, length = 2)
    private String bbnAcctStatus;

    @Column(name = "bbn_beban_status", unique = false, nullable = false, insertable = true, updatable = true, length = 2)
    private String bbnBebanStatus;

    @Column(name = "bbn_ambc_prin", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAmbcPrin = BigDecimal.ZERO;

    @Column(name = "bbn_ambc_int", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAmbcInt = BigDecimal.ZERO;

    @Column(name = "bbn_duedate", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnDuedate;

    @Column(name = "bbn_paid_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnPaidDate;

    @Column(name = "bbn_trx_codeid", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnTrxCodeid;

    @Column(name = "bbn_paymethode_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnPaymethodeId;

    @Column(name = "bbn_ac_prin", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAcPrin = BigDecimal.ZERO;

    @Column(name = "bbn_ac_int", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAcInt = BigDecimal.ZERO;

    @Column(name = "bbn_ac_penalty", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAcPenalty = BigDecimal.ZERO;

    @Column(name = "bbn_ac_collfee", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnAcCollfee = BigDecimal.ZERO;

    @Column(name = "bbn_date_pu", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnDatePu;

    @Column(name = "bbn_date_ayd", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnDateAyd;

    @Column(name = "bbn_opr_acprin", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOprAcprin = BigDecimal.ZERO;

    @Column(name = "bbn_opr_acint", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnOprAcint = BigDecimal.ZERO;

    @CreatedDate
    @Column(name = "bbn_created_date", unique = false, nullable = false, insertable = true, updatable = true)
    private Date bbnCreatedDate;

    @LastModifiedDate
    @Column(name = "bbn_updated_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnUpdatedDate;

    @CreatedBy
    @Column(name = "bbn_created_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String bbnCreatedBy = "ADMIN";

    @LastModifiedBy
    @Column(name = "bbn_updated_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String bbnUpdatedBy;

    @Column(name = "bbn_collector_type", unique = false, nullable = true, insertable = true, updatable = true)
    private String bbnCollectorType;

    @Column(name = "bbn_collector_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnCollectorId;

    @Column(name = "bbn_supervisor_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnSupervisorId;

    @Column(name = "bbn_last_coll_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnLastCollId;

    @Column(name = "bbn_last_supervisor_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnLastSupervisorId;

    @Column(name = "bbn_pitstop", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    private String bbnPitstop;

    @Column(name = "bbn_flag_pkpc", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnFlagPkpc = "N";

    @Column(name = "bbn_ambc_prin_akhir", unique = false, nullable = true, insertable = true, updatable = true, length = 16)
    private BigDecimal bbnAmbcPrinAkhir;

    @Column(name = "bbn_ambc_int_akhir", unique = false, nullable = true, insertable = true, updatable = true, length = 16)
    private BigDecimal bbnAmbcIntAkhir;

    @Column(name = "bbn_last_deskcall_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnLastDeskcallId;

    @Column(name = "bbn_lkp_status", unique = false, nullable = true, insertable = true, updatable = true, length = 2)
    private String bbnLkpStatus;

    @Column(name = "bbn_lkp_last_visit", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnLkpLastVisit;

    @Column(name = "bbn_lkp_last_flag", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnLkpLastFlag;

    @Column(name = "bbn_last_lkp_no", unique = false, nullable = true, insertable = true, updatable = true, length = 25)
    private String bbnLastLkpNo;

    @Column(name = "bbn_last_lkp_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnLastLkpDate;

    @Column(name = "bbn_next_visit", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnNextVisit;

    @Column(name = "bbn_last_potensi", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnLastPotensi;

    @Column(name = "bbn_visited", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnVisited;

    @Column(name = "bbn_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 25)
    private String bbnZip;

    @Column(name = "bbn_sub_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 25)
    private String bbnSubZip;

    @Column(name = "bbn_last_promise_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnLastPromiseDate;

    @Column(name = "bbn_assign_date_first", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnAssignDateFirst;

    @Column(name = "bbn_assign_date_last", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnAssignDateLast;

    @Column(name = "bbn_work_flag", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnWorkFlag;

    @Column(name = "bbn_instl_type", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnInstlType;

    @Column(name = "bbn_collected_by", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnCollectedBy;

    @Column(name = "bbn_ac_prnc_field", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnAcPrncField;

    @Column(name = "bbn_ac_intr_field", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnAcIntrField;

    @Column(name = "bbn_last_coll_prevmonth", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnLastCollPrevmonth;

    @Column(name = "bbn_polabayar_prevmonth", unique = false, nullable = true, insertable = true, updatable = true, length = 2)
    private String bbnPolabayarPrevmonth;

    @Column(name = "bbn_cycle_awal_prevmonth", unique = false, nullable = true, insertable = true, updatable = true, length = 2)
    private String bbnCycleAwalPrevmonth;

    @Column(name = "bbn_last_plan_code", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    private String bbnLastPlanCode;

    @Column(name = "bbn_last_delq_code", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnLastDelqCode;

    @Column(name = "bbn_type_lks", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnTypeLks;

    @Column(name = "bbn_jrk_angs", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnJrkAngs;

    @Column(name = "bbn_prio_flag", unique = false, nullable = true, insertable = true, updatable = true, length = 5)
    private String bbnPrioFlag;

    @Column(name = "bbn_prio_seqn", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnPrioSeqn;

    @Column(name = "bbn_penugasan", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnPenugasan;

    @Column(name = "bbn_temp_assign", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnTempAssign = "N";

    @Column(name = "bbn_flag_somasi", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnFlagSomasi = "N";

    @Column(name = "bbn_maturity_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnMaturityDate;

    @Column(name = "bbn_booked_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date bbnBookedDate;

    @Column(name = "bbn_catg_lks", unique = false, nullable = true, insertable = true, updatable = true, length = 2)
    private String bbnCatgLks;

    @Column(name = "bbn_coll_id_prevmonth", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnCollIdPrevmonth;

    @Column(name = "bbn_ready_assign", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnReadyAssign = "N";

    @Column(name = "bbn_coll_id_bef_lks", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnCollIdBefLks;

    @Column(name = "bbn_bisnis_unit", unique = false, nullable = true, insertable = true, updatable = true, length = 10)
    private String bbnBisnisUnit;

    @Column(name = "bbn_pc_month_assign", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnPcMonthAssign;

    @Column(name = "bbn_bucket_type", unique = false, nullable = true, insertable = true, updatable = true, length = 3)
    private String bbnBucketType;

    @Column(name = "bbn_supervisor_id_bef", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnSupervisorIdBef;

    @Column(name = "bbn_coll_id_bef", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnCollIdBef;

    @Column(name = "bbn_visit_bef_hc", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnVisitBefHc;

    @Column(name = "bbn_pc_sw_id", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String bbnPcSwId;

    @Column(name = "bbn_assigned", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnAssigned;

    @Column(name = "bbn_coll_fee", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnCollFee = BigDecimal.ZERO;

    @Column(name = "bbn_denda_tetap", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnDendaTetap = BigDecimal.ZERO;

    @Column(name = "bbn_denda_berjalan", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnDendaBerjalan = BigDecimal.ZERO;

    @Column(name = "bbn_prd_sharia_convent", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnPrdShariaConvent;

    @Column(name = "bbn_paymethode", unique = false, nullable = true, insertable = true, updatable = true, length = 50)
    private String bbnPaymethode;

    @Column(name = "bbn_rv_system", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    private String bbnRvSystem;

    @Column(name = "bbn_last_class_code", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnLastClassCode;

    @Column(name = "bbn_interest_type", unique = false, nullable = true, insertable = true, updatable = true, length = 1)
    private String bbnInterestType;

    @Column(name = "bbn_central_orgid", unique = false, nullable = true, insertable = true, updatable = true, length = 20)
    private String bbnCentralOrgid;

    @Column(name = "bbn_product_shortname", unique = false, nullable = true, insertable = true, updatable = true)
    private String bbnProductShortname;

    @Column(name = "bbn_min_payment", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnMinPayment;

    @Column(name = "bbn_set_minpinalty", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnSetMinpinalty;

    @Column(name = "bbn_set_mincollfee", unique = false, nullable = true, insertable = true, updatable = true)
    private BigDecimal bbnSetMincollfee;

    @Column(name = "bbn_coll_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 25)
    private String bbnCollZip;

    @Column(name = "bbn_coll_sub_zip", unique = false, nullable = true, insertable = true, updatable = true, length = 25)
    private String bbnCollSubZip;

    @Column(name = "bbn_ao_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnAoId;

    @Column(name = "bbn_upload_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer bbnUploadId;

}
