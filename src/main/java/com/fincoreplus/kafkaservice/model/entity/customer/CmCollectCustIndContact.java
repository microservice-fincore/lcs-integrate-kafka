package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cm_collect_cust_indcontact", schema = "staging")
public class CmCollectCustIndContact {

    @EmbeddedId
    CmCollectCustIndContractKey key;

    @Column(name = "cont_type_id")
    private Integer typeId;

    @Column(name = "cont_text")
    private String text;

    @CreatedBy
    @Column(name = "cont_created_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String createdBy;

    @CreatedDate
    @Column(name = "cont_created_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "cont_updated_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "cont_updated_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date updatedDate;

    @Column(name = "acct_upload_id", unique = false, nullable = true, insertable = true, updatable = true)
    private Integer acctUploadId;

}
