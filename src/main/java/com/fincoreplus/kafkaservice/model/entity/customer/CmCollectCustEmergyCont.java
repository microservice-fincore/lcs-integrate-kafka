package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "cm_collect_cust_emergycont", schema = "staging")
public class CmCollectCustEmergyCont {

    @EmbeddedId
    CmCollectCustEmergyContKey key;

    @Column(name = "cemg_name", length = 60)
    private String name;

    @Column(name = "cemg_rel_id")
    private Integer relId;

    @Column(name = "cemg_addr")
    private String address;

    @Column(name = "cemg_rt", length = 5)
    private String rt;

    @Column(name = "cemg_rw", length = 5)
    private String rw;

    @Column(name = "cemg_kelid")
    private Integer kelId;

    @Column(name = "cemg_kecid")
    private Integer kecId;

    @Column(name = "cemg_kabkotaid")
    private Integer kotaId;

    @Column(name = "cemg_provid")
    private Integer provId;

    @Column(name = "cemg_zip", length = 5)
    private String zip;

    @Column(name = "cemg_area_ph", length = 5)
    private String areaPh;

    @Column(name = "cemg_phone", length = 30)
    private String phone;

    @Column(name = "cemg_mblphone", length = 30)
    private String mobilePhone;

    @CreatedBy
    @Column(name = "cemg_created_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String createdBy;

    @CreatedDate
    @Column(name = "cemg_created_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "cemg_updated_by", unique = false, nullable = true, insertable = true, updatable = true, length = 30)
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "cemg_updated_date", unique = false, nullable = true, insertable = true, updatable = true)
    private Date updatedDate;

}
