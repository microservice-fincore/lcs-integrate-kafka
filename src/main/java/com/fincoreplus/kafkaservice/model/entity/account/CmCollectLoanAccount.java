package com.fincoreplus.kafkaservice.model.entity.account;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : bcoll-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2/12/24
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */

@Getter
@Setter
@Entity
@Builder
@Table(name = "cm_collect_loan_account", schema = "staging")
@AllArgsConstructor
@NoArgsConstructor
public class CmCollectLoanAccount {
    @EmbeddedId
    private CmCollectLoanAccountKey id;

    @Size(max = 64)
    @Column(name = "acct_name", length = 64)
    private String acctName;

    @Size(max = 3)
    @Column(name = "acct_cust_type", length = 3)
    private String acctCustType;

    @Size(max = 20)
    @Column(name = "acct_cust_id", length = 20)
    private String acctCustId;

    @Column(name = "acct_active_date")
    private LocalDate acctActiveDate;

    @Size(max = 3)
    @Column(name = "acct_currency", length = 3)
    private String acctCurrency;

    @Size(max = 20)
    @Column(name = "acct_order_no", length = 20)
    private String acctOrderNo;

    @Column(name = "acct_loanakad_doc")
    @JdbcTypeCode(SqlTypes.JSON)
    private Map<String, Object> acctLoanakadDoc;

    @Column(name = "acct_created_by", nullable = false)
    private String acctCreatedBy;

    @NotNull
    @Column(name = "acct_created_date", nullable = false)
    private OffsetDateTime acctCreatedDate;

    @Column(name = "acct_updated_by")
    private String acctUpdatedBy;

    @Column(name = "acct_updated_date")
    private OffsetDateTime acctUpdatedDate;

    @Column(name = "acct_version_no")
    private Integer acctVersionNo;

    @Column(name = "acct_project_amt", precision = 18, scale = 2)
    private BigDecimal acctProjectAmt;

    @Column(name = "acct_loan_amt", precision = 18, scale = 2)
    private BigDecimal acctLoanAmt;

    @Column(name = "acct_int_or_margin", precision = 18, scale = 2)
    private BigDecimal acctIntOrMargin;

    @Column(name = "acct_pct_ovdpen", precision = 7, scale = 2)
    private BigDecimal acctPctOvdpen;

    @Column(name = "acct_min_ovdpen", precision = 18, scale = 2)
    private BigDecimal acctMinOvdpen;

    @Column(name = "acct_ovd_toldays")
    private Integer acctOvdToldays;

    @Column(name = "acct_pctadm_preterm", precision = 7, scale = 2)
    private BigDecimal acctPctadmPreterm;

    @Column(name = "acct_maturity_date")
    private LocalDate acctMaturityDate;

    @Column(name = "acct_pct_flat", precision = 7, scale = 4)
    private BigDecimal acctPctFlat;

    @Column(name = "acct_pct_eff", precision = 7, scale = 4)
    private BigDecimal acctPctEff;

    @Column(name = "acct_jml_angs")
    private Integer acctJmlAngs;

    @Column(name = "acct_jarak_angs")
    private Integer acctJarakAngs;

    @Size(max = 1)
    @Column(name = "acct_period_type", length = 1)
    private String acctPeriodType;

    @Column(name = "acct_install_amt", precision = 18, scale = 2)
    private BigDecimal acctInstallAmt;

    @Column(name = "acct_last_instamt", precision = 18, scale = 2)
    private BigDecimal acctLastInstamt;

    @Column(name = "acct_tazir_amt", precision = 18, scale = 2)
    private BigDecimal acctTazirAmt;

    @Column(name = "acct_pct_provisi", precision = 7, scale = 2)
    private BigDecimal acctPctProvisi;

    @Column(name = "acct_pct_loaninsc", precision = 18, scale = 2)
    private BigDecimal acctPctLoaninsc;

    @Column(name = "acct_loan_assets")
    @JdbcTypeCode(SqlTypes.JSON)
    private Map<String, Object> acctLoanAssets;

    @Size(max = 2)
    @Column(name = "acct_ad_ar_flag", length = 2)
    private String acctAdArFlag;

    @Size(max = 1)
    @Column(name = "acct_instl_type", length = 1)
    private String acctInstlType;

    @Column(name = "acct_ambc_prnc")
    private BigDecimal acct_ambc_prnc;

    @Column(name = "acct_ambc_intr")
    private BigDecimal acct_ambc_intr;

    @Column(name = "acct_install_no")
    private Integer acct_install_no;

    @Column(name = "acct_upload_id")
    private Integer acctUploadId;

}
