package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class CmCollectCustAddrKey implements Serializable {
    @Column(name = "caddr_cust_id", unique = false, nullable = false, insertable = true, updatable = true, length = 20)
    private String caddrCustId;

    @Column(name = "caddr_company_id", unique = false, nullable = false, insertable = true, updatable = true)
    private Integer caddrCompanyId;

    @Column(name = "caddr_diffid", unique = false, nullable = false, insertable = true, updatable = true)
    private Integer caddrDiffid;
}
