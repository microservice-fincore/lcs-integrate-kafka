package com.fincoreplus.kafkaservice.model.entity.account;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * Project : bcoll-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2/12/24
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */

@Getter
@Setter
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class CmCollectLoanAccountKey implements Serializable {
    private static final long serialVersionUID = -2459019202667138289L;
    @Size(max = 16)
    @NotNull
    @Column(name = "acct_no", nullable = false, length = 16)
    private String acctNo;

    @NotNull
    @Column(name = "acct_company_id", nullable = false)
    private Integer acctCompanyId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CmCollectLoanAccountKey entity = (CmCollectLoanAccountKey) o;
        return Objects.equals(this.acctNo, entity.acctNo) &&
                Objects.equals(this.acctCompanyId, entity.acctCompanyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(acctNo, acctCompanyId);
    }

}
