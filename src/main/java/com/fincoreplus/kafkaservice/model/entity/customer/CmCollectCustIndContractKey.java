package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class CmCollectCustIndContractKey {

    @Column(name = "cont_cust_id", nullable = false, length = 20)
    private String custId;

    @Column(name = "cont_company_id", nullable = false)
    private Integer companyId;

    @Column(name = "cont_diffid", nullable = false)
    private Integer diffId;
}
