package com.fincoreplus.kafkaservice.model.entity.bucket;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class CmCollectBucketKey implements Serializable {

    @Column(name = "bbn_year", unique = true, nullable = false, insertable = true, updatable = true)
    private Integer bbnYear;

    @Column(name = "bbn_account_no", unique = true, nullable = false, insertable = true, updatable = true, length = 16)
    private String bbnAccountNo;

    @Column(name = "bbn_company_id", unique = true, nullable = false, insertable = true, updatable = true)
    private Integer bbnCompanyId;

    @Column(name = "bbn_month", unique = true, nullable = false, insertable = true, updatable = true)
    private Integer bbnMonth;
}
