package com.fincoreplus.kafkaservice.model.entity.customer;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class CmCollectCustEmergyContKey {

    @Column(name = "cemg_custid", length = 20)
    private String custId;

    @Column(name = "cemg_company_id")
    private Integer companyId;
}
