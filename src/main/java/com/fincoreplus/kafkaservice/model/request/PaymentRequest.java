package com.fincoreplus.kafkaservice.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRequest implements Serializable {
    private Integer year;
    private Integer month;
    private String company_code;
    private String reff_no;
    private String response_message;
    private boolean response_success;
    private boolean core_payment;
    private List<PaymentAccountRequest> data;
}
