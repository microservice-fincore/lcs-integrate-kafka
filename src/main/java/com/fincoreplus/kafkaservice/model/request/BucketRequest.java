package com.fincoreplus.kafkaservice.model.request;

import com.fincoreplus.kafkaservice.model.Bucket;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BucketRequest {
    private String trx_id;
    private Date trx_date;
    private String company_code;
    private int year;
    private int month;
    private List<Bucket> data;
}
