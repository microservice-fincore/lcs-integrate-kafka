package com.fincoreplus.kafkaservice.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentAccountRequest {
    private String contract_no;
    private BigDecimal ac_interest;
    private BigDecimal ac_principal;
    private BigDecimal ac_penalty;
    private BigDecimal ac_fee;
    private BigDecimal payment_amount;
}
