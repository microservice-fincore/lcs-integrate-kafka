package com.fincoreplus.kafkaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class KafkaServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(KafkaServiceApp.class, args);
    }

}
